﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bit4you.Models;
using Bit4you.Models.Market.Request;
using Bit4you.Models.Market.Response;
using Bit4you.Models.Orders.Request;
using Bit4you.Models.Orders.Response;
using Bit4you.Models.Portfolio;
using Bit4you.Models.Portfolio.Request;
using Bit4you.Models.Portfolio.Response;
using Bit4you.Models.Wallet;
using Bit4you.Models.Wallet.Request;
using Bit4you.Models.Wallet.Response;
using Bit4you.Services;

namespace BitForYouCSharpClient.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Bit4YouClientController : ControllerBase
    {

        private readonly ILogger<Bit4YouClientController> _logger;
        private readonly IBit4YouClient _bit4YouClient;
        private readonly IHttpClientCustomFactory _httpClientCustomFactory;

        public Bit4YouClientController(ILogger<Bit4YouClientController> logger, IBit4YouClient bit4YouClient, IHttpClientCustomFactory httpClientCustomFactory)
        {
            _logger = logger;
            _bit4YouClient = bit4YouClient;
            _httpClientCustomFactory = httpClientCustomFactory;
        }


        #region market
        [HttpGet("marketList")]
        public async Task<IEnumerable<MarketListResponse>> marketList()
        {
            return await _bit4YouClient.MarketList();
        }

        [HttpGet("marketSummaries")]
        public async Task<IEnumerable<MarketSummrie>> marketSummaries()
        {
            return await _bit4YouClient.MarketSummaries();
        }

        [HttpGet("marketTics")]
        public async Task<IEnumerable<MarketTicksResponse>> marketTics(MarketTicks marketticks)
        {
            return await _bit4YouClient.MarketTicks(marketticks);
        }

        [HttpGet("marketOrderBooks")]
        public async Task<MarketOrderBookResponse> marketOrderBooks(MarketOrderBook orderbook)
        {
            return await _bit4YouClient.MarketOrderBook(orderbook);
        }

        [HttpGet("marketHistory")]
        public async Task<List<MarketHistoryResponse>> marketHistory(MarketHistory markethistory)
        {
            return await _bit4YouClient.MarketHistory(markethistory);
        }


        #endregion

        #region User
        [HttpGet("User")]
        public async Task<UserInfo> UserInfo()
        {
            return await _bit4YouClient.GetUserInfoAsync();
        }


        #endregion

        #region Wallet
        [HttpGet("WalletBalance")]
        public async Task<List<WalletBalanceResponse>> WalletBalance()
        {
            return await _bit4YouClient.WalletBalance(new Simulations()
            {
                Simulation = true,
                //ClientId = "string",
                //TimingForce = ‘DAY’,‘GTC’,‘OPG’,‘GTD’,‘IOC’,‘FOK’

            });
        }

        [HttpGet("WalletTransactions")]
        public async Task<WalletTransactionResponse> WalletTransactions()
        {
            return await _bit4YouClient.WalletTransaction(new WalletTransaction()
            {
                Simulation = true,
                Iso = "BTC",
                //ClientId = "string",
                //TimingForce = ‘DAY’,‘GTC’,‘OPG’,‘GTD’,‘IOC’,‘FOK’
            });
        }

        [HttpGet("WalletSend")]
        public async Task WalletSend()
        {
            await _bit4YouClient.WalletWithdrawFunds(new WalletFunds()
            {
                Iso = "BTC",
                Address = "1CK6KHY6MHgYvmRQ4PAafKYDrg1eaaaaaa",
                Quantity = 1.05,
                //ClientId = "string",
                //TimingForce = ‘DAY’,‘GTC’,‘OPG’,‘GTD’,‘IOC’,‘FOK’
            });
        }


        #endregion

        #region Order
        [HttpPost("OrderList")]
        public async Task<List<OrderListResponse>> OrderList(OrderList order)
        {
            return await _bit4YouClient.OrderList(order);
        }

        [HttpPost("OrderInfo")]
        public async Task<OrderInfoResponse> OrderInfo(OrderInfo orderinfo)
        {
            
           return  await _bit4YouClient.OrderInfo(orderinfo);
        }

        [HttpPost("OrderPending")]
        public async Task<List<OrderPendingResponse>> OrderPending(OrderPending orderpending)
        {

           return await _bit4YouClient.OrderPending(orderpending);
        }

        [HttpPost("OrderCreate")]
        public async Task<OrderCreateResponse> OrderCreate(OrderCreate ordercreate)
        {

           return await _bit4YouClient.OrderCreate(ordercreate);
        }

        [HttpPut("CancelOrder")]
        public async Task<OrderCancelResponse> CencelOrder(OrderCancel ordercancel)
        {

           return await _bit4YouClient.OrderCancel(ordercancel);
        }


        #endregion

        #region Portfolio
        [HttpGet("PortfolioSummary")]
        public async Task<PortfolioListResponse> PortfolioSummary(Simulations simulation)
        {
           return  await _bit4YouClient.PortfolioSummary(simulation);
        }
        [HttpGet("PortfolioOpenOrder")]
        public async Task<List<PortfolioOpenOrderResponse>> PortfolioOpenOrder(Simulations simulation)
        {
            return await _bit4YouClient.PortfolioOpenOrder(simulation);
        }

        [HttpGet("PortfolioHistory")]
        public async Task<List<PortfolioHistoryResponse>> PortfolioHistory(Simulations simulation)
        {
            return await _bit4YouClient.PortfolioHistory(simulation);
        }

        [HttpPost("PortfolioCreateOrder")]
        public async Task<PortfolioCreateResponse> PortfolioCreateOrder(PortfolioCreateOrder createorder)
        {
            return await _bit4YouClient.PortfolioCreateOrder(createorder);
        }

        [HttpPut("PortfolioCancelOrder")]
        public async Task<PortfolioCancelResponse> PortfolioCancelOrder(PortfolioCancelOrder cancelorder)
        {
            return await _bit4YouClient.PortfolioCancelOrder(cancelorder);
        }

        [HttpPut("PortfolioCloseOrder")]
        public async Task PortfolioCloseOrder(PortfolioClosePosition closeposition)
        {
             await _bit4YouClient.PortfolioCloseOrder(closeposition);
        }

        #endregion

    }
}
