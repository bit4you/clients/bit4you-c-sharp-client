﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bit4you.Constants
{
    public static class TimeInForce
    {
        public static string FOK = "FOK";
        public static string DAY = "DAY";
        public static string GTC = "GTC";
        public static string OPG = "OPG";
        public static string GTD = "GTD";
        public static string IOC = "IOC";
    }
}
