﻿namespace Bit4you.Models.Orders.Response
{
    public class OrderCancelResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
