﻿namespace Bit4you.Models.Portfolio.Request
{
    public class PortfolioCreateOrder : Simulations
    {
        public string Market { get; set; }
        public double Quantity { get; set; }
        public double Rate { get; set; }
        public string ClientId { get; set; }
        public string TimeInForce { get; set; }
    }
}
