﻿namespace Bit4you.Models.Portfolio.Response
{
    public class PortfolioCancelResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
