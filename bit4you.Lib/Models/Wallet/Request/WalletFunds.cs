﻿namespace Bit4you.Models.Wallet.Request
{
    public class WalletFunds
    {
        public string Iso { get; set; }
        public double Quantity { get; set; }
        public string Address { get; set; }
    }
}
