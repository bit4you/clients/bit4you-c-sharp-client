﻿namespace Bit4you.Models.Market.Request
{
    public class MarketTicks 
    {
        public string Market { get; set; }
        public int Interval { get; set; }
    }
}
