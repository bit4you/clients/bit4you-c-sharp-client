﻿namespace Bit4you.Models
{
    public enum ApiAction
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
