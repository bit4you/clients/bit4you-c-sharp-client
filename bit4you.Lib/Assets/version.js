const https = require("https")
const zlib = require("zlib")
const fs = require("fs")
var path = require('path');


function getVersion() {
    return new Promise(async (resolve, reject) => {
      
        const req = https.request("https://api.nuget.org/v3/registration5-gz-semver2/bit4you/index.json" , (res) => {
            var body = []
      
            res.on('data', function(chunk) {
                body.push(chunk)
            })
            res.on('end', function() {

                zlib.gunzip(Buffer.concat(body), function(err, decoded) {
                    if(err)
                        reject(err)

                    try {
                        decoded = JSON.parse(decoded.toString())
                        var version = decoded.items[0].upper.split(".")
                        version[2] = +version[2] + 1
                        resolve(version.join("."))

                    } catch(e) {
                        reject(e)
                    }
                })
            })
        })
        req.on('error', (e) => {
            reject(e.message)
        })
        req.end()
    })
}

async function replaceVersion(version) {
    var  file = path.resolve(__dirname , "../bit4you.Lib.csproj");
    console.log("Replacing version to " , version , "file" ,  file)
    return new Promise((resolve ,reject) => {
        fs.readFile(file, function(err,content){
            if(err) throw err;
            content = content.toString("utf-8").replace(/<Version>[\s\S]*?<\/Version>/, '<Version>' + version + '<\/Version>');
            fs.writeFile(file, content, () => {
                console.log("Replacing done")
                resolve();
            })
       
            
        });
    })
    
    
    
}

return getVersion().then(async version => {
    await replaceVersion(version)
    process.exit()
}).catch(x => {
    throw x
})
