﻿using System.Threading.Tasks;
using Bit4you.Models;

namespace Bit4you.Services
{
    public interface IOAuthClient
    {
        Task<OAuthResult> GetAccessTokenAsync();
    }

    
}
