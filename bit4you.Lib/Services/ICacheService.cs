﻿using System.Threading.Tasks;
using Bit4you.Models;

namespace Bit4you.Services
{
    public interface ICacheService
    {
        OAuthResult GetAuthenticationResultFromCache(ClientSettings clientSettings);
        void Add<T>(T o, string key, int timeInSeconds);
    }
}
