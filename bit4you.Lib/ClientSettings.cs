﻿namespace Bit4you
{
    public class ClientSettings
    {
        public string AccessTokenUrl { get; set; }
        public string ApiUrl { get; set; }
        public string UserInfo { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string[] OAuthScopes { get; set; }

        public ClientSettings()
        {
            AccessTokenUrl = "https://auth.bit4you.io/";
            ApiUrl = "https://www.bit4you.io/api/";
            UserInfo = "https://auth.bit4you.io";

        }
    }
}
